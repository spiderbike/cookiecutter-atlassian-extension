using Amazon;
using Atlassian.Connect;
using Atlassian.Connect.Lifecycle.Models;
using Atlassian.Connect.TenantRepository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Atlassian.Connect.Client;

namespace Jira_extension_test1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public static IConfiguration Configuration { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAtlassianConnectServices();
            services.AddAtlassianConnectClient();



            services.AddOptions<AtlassianConnectOptions>()
                .Configure(opt =>
                {
                    opt.InstallCallbackPath = "/installed";
                    opt.UninstallCallbackPath = "/uninstalled";
                    opt.EnabledCallbackPath = "/enabled";
                    opt.DisabledCallbackPath = "/disabled";
                    opt.DescriptorPath = "/atlassian-connect.json";
                    opt.AddOnKey = "com.spiderbike.local.TestLambda";

                    opt.Descriptor = new AddOnDescriptor
                    {
                        Name = "test jira lambda example",
                        Description = "Atlassian Connect add-on, that shows the usage of data in attachments on your Jira instance over time.",
                        Vendor = new Vendor() { Name = "Acme Software", Url = new Uri("http://www.acme.com") },
                        Authentication = new Authentication
                        {
                            Type = AuthenticationType.jwt
                        },
                        ApiVersion = 1,
                        EnableLicensing = true,
                        Modules = new Modules
                        {
                            GeneralPages = new[]
                            {
                                new PageModule
                                {
                                    Url = "/api/values",
                                    Key = "values-home",
                                    Location = "system.top.navigation.bar",
                                    Name = new I18nProperty {Value = "Values"}
                                }
                            }
                        },

                        Scopes = new List<Scopes>
                        {
                            Scopes.read
                        }
                    };
                });

            services.AddSingleton<ITenantRepository>(new AwsSecretsManagerTenantRepository($"{GetType().Namespace}-2", RegionEndpoint.EUWest1));
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapAtlassianLifecycleEndpoints();
                endpoints.MapControllers();
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Welcome to running ASP.NET Core on AWS Lambda");
                });
            });
        }


    }
}
